# Slackware15-Reposity Electron
*Electron-based programs slackbuilds for Slackware 15.0 Stable ( x86_64 / i586 ).*

SlackBuilds Repository specific for electron-based programs.

## Discord Installation

https://discord.com/

All-in-one voice and text chat for gamers that's free, secure, and works on both your desktop and phone.<br/>
Stop paying for TeamSpeak servers and hassling with Skype. Simplify your life.<br/>
Install the following SlackBuild:
```
- discord.SlackBuild
```

## IPTVnator Installation

https://github.com/4gray/iptvnator

IPTVnator is a video player application that provides support for the playback of IPTV playlists (m3u, m3u8).<br/>
The application allows to import playlists by using remote URLs or per file upload from the file system.<br/>
Install the following SlackBuild:
```
- iptvnator.SlackBuild
```

## Notable Installation

https://notable.app/

Notable is a bare-bones note taking application with excellent Markdown support. <br/>
Free for desktop use, Notable is designed for people who like to see Markdown-formatted text while they’re typing.<br/>
Install the following SlackBuild:
```
- notable.SlackBuild
```

## VSCodium Installation

https://vscodium.com/

VSCodium is a community-driven, freely-licensed binary distribution of Microsoft’s editor VSCode. <br/>
Install the following SlackBuild:
```
- vscodium.SlackBuild
```
