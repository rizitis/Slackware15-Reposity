# Slackware15-Reposity Games
*Game slackbuilds for Slackware 15.0 Stable ( x86_64 / i586 ).*

SlackBuilds Repository specific for Games.

## Flare Installation

https://flarerpg.org/

Flare is an open source, 2D action RPG licensed under the GPL3 license.<br/>
Its game play can be likened to the games in the Diablo series.<br/>
To work, install the following SlackBuilds:
```
- flare-game.SlackBuild (Include Engine)
```

## OpenSonic Installation

http://opensnc.sourceforge.net/

Open Sonic is an open-source game based on the "Sonic the Hedgehog" universe.<br/>
It introduces a different style of gameplay called cooperative play, in which it's possible to control 3 characters simultaneously.<br/>
To work, install the following SlackBuilds:
```
- allegro4.SlackBuild

- opensonic.SlackBuild ---> Game
```

## Secret Maryo Chronicles Installation

http://www.secretmaryo.org/

Secret Maryo Chronicles is an Open Source two-dimensional platform game with a design similar to classic computer games.<br/>
SMC uses an accelerated Open Graphics Library to support high quality textures and performance.<br/>
To work, install the following SlackBuilds:
```
- CEGUI07.SlackBuild
- FreeImage.SlackBuild (CEGUI07 dependency)

- smc.SlackBuild ---> Game
```

## Yamagi Quake II Installation

https://www.yamagi.org/quake2/

Yamagi Quake II is an alternative client for id Softwares Quake II.<br/>
To work, install the following SlackBuilds:
```
- quake2.SlackBuild
```
