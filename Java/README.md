# Slackware15-Reposity Java
*Java-based programs slackbuilds for Slackware 15.0 Stable ( x86_64 / i586 ).*

SlackBuilds Repository specific for java-based programs.

## CLion Installation

https://www.jetbrains.com/clion/

A cross-platform IDE for C and C++.<br/>
CLion uses the project model to inform its coding assistance, refactoring, coding style consistency, and other smart actions in the editor.<br/>
Install the following SlackBuild:
```
- clion.SlackBuild
```

## FrostWire Installation

https://www.frostwire.com/

Cloud Downloader, BitTorrent Client and Media Player.<br/>
Easily search and download files directly from the bittorrent network and cloud sources.<br/>
Use frostwire's built-in media library to access and play your downloads - all in one simple app.<br/>
Install the following SlackBuild:
```
- jdk16.SlackBuild
- frostwire.SlackBuild
```

## JDownloader2 Installation

https://jdownloader.org/jdownloader2

JDownloader is a free, open-source download management tool with a huge community of developers that makes downloading as easy and fast as it should be.<br/>
Users can start, stop or pause downloads, set bandwith limitations, auto-extract archives and much more. <br/>
Install the following SlackBuild:
```
- jdk16.SlackBuild
- JDownloader2.SlackBuild
```

## Java Installation

https://www.oracle.com/br/java/technologies/javase-jdk16-downloads.html

These packages need "Java" to work, except "CLion" as it already has Java included.<br/>
If you don't have Java installed, use this version:
```
- jdk16.SlackBuild
```

## PyCharm Installation

https://www.jetbrains.com/pycharm/

The Python IDE for Professional Developers.<br/>
With PyCharm, you can access the command line, connect to a database, create a virtual environment, and manage your version control system all in one place.<br/>
Install the following SlackBuild:
```
- pycharm.SlackBuild
```
