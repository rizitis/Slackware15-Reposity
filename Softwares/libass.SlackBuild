#!/bin/sh
# Dependencies

# Slackware build script for libass

# Copyright 2021 Mauricio Ferrari <m10ferrari1200@gmail.com>
# All rights reserved.

[ $UID != 0 ] && { echo -e "\nExecute como Root !\n"; exit 1; }

PRGNAM=libass
VERSION=0.15.2
BUILD=${BUILD:-1}
TAG=${TAG:-_mxnt}

case "$( uname -m )" in
	i?86) ARCH=i586 ;;
	arm*) ARCH=arm ;;
	   *) ARCH=$( uname -m ) ;;
esac

WD=$PWD; mkdir -p $PRGNAM; chown -R 1000:users $PRGNAM; cd $PRGNAM 

CWD=$PWD
TMP=/tmp
PKG=$TMP/package-$PRGNAM
LINK=https://github.com/libass/libass/releases/download/0.15.2/libass-0.15.2.tar.xz

if [ "$ARCH" = "i586" ]; then
	SLKCFLAGS="-O2 -march=i586 -mtune=i686"
	LIBDIRSUFFIX=""
elif [ "$ARCH" = "i686" ]; then
	SLKCFLAGS="-O2 -march=i686 -mtune=i686"
	LIBDIRSUFFIX=""
elif [ "$ARCH" = "aarch64" ]; then
	SLKCFLAGS="-O2"
	LIBDIRSUFFIX="64"
elif [ "$ARCH" = "x86_64" ]; then
	SLKCFLAGS="-O2 -fPIC"
	LIBDIRSUFFIX="64"
else
	SLKCFLAGS="-O2"
	LIBDIRSUFFIX=""
fi

set -e
wget -c $LINK
rm -rf $PKG $TMP/$PRGNAM-$VERSION
mkdir -p $PKG/{install,usr/doc/$PRGNAM-$VERSION}
cd $TMP; tar xvf $CWD/$PRGNAM-$VERSION.tar.?z
cd $PRGNAM-$VERSION
chown -R root:root .
chmod -R u+w,go+r-w,a+X-s .

CFLAGS="$SLKCFLAGS" \
CXXFLAGS="$SLKCFLAGS" \
./configure \
  --prefix=/usr \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --enable-static=no \
  --disable-silent-rules \
  --build=$ARCH-slackware-linux
make; make install-strip DESTDIR=$PKG

cp -a Changelog COPYING $PKG/usr/doc/$PRGNAM-$VERSION

echo "      |-----handy-ruler------------------------------------------------------|
libass: libass (Subtitle renderer for the ASS/SSA)
libass:
libass: libass is a portable subtitle renderer for the ASS/SSA (Advanced
libass: Substation Alpha/Substation Alpha) subtitle format. It is mostly
libass: compatible with VSFilter.
libass:
libass: Homepage: http://code.google.com/p/libass/
libass:
libass:
libass:
libass:" > $PKG/install/slack-desc

cd $PKG; /sbin/makepkg -l y -c n $CWD/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.txz
chown -R 1000:users $CWD/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.txz
rm -rf $PKG $TMP/$PRGNAM-$VERSION

cd $CWD
[ ${TIME:-0} != 0 ] && TIME="-t $TIME" || TIME=
if [ "${INST:-no}" = "yes" ]; then
	OPTION=y
else
	[ -e "../Exclamation.mp3" ] && pkexec env DISPLAY=$DISPLAY XAUTHORITY=$XAUTHORITY mplayer $WD/Exclamation.mp3 > /dev/null
	read $TIME -p "O pacote já pode ser instalado? (y/n) (default=n)" OPTION
fi
case "$OPTION" in
	y|Y) /sbin/upgradepkg --install-new --reinstall $PRGNAM-$VERSION-$ARCH-$BUILD$TAG.txz ;;
esac; exit 0
