#!/bin/sh
# Dependencies

# Slackware build script for fuseiso

# Copyright 2021 Mauricio Ferrari <m10ferrari1200@gmail.com>
# All rights reserved.

[ $UID != 0 ] && { echo -e "\nExecute como Root !\n"; exit 1; }

PRGNAM=fuseiso
VERSION=20070708
BUILD=${BUILD:-1}
TAG=${TAG:-_mxnt}

case "$( uname -m )" in
	i?86) export ARCH=i586 ;;
	arm*) export ARCH=arm ;;
	   *) export ARCH=$( uname -m ) ;;
esac

WD=$PWD; mkdir -p $PRGNAM; chown -R 1000:users $PRGNAM; cd $PRGNAM 

CWD=$PWD
TMP=/tmp
PKG=$TMP/package-$PRGNAM
LINK=https://ufpr.dl.sourceforge.net/project/fuseiso/fuseiso/20070708/fuseiso-20070708.tar.bz2

if [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
elif [ "$ARCH" = "i686" ]; then
  SLKCFLAGS="-O2 -march=i686 -mtune=i686"
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
fi

set -e
wget -c $LINK
rm -rf $PKG $TMP/$PRGNAM-$VERSION
mkdir -p $PKG/{install,usr/doc/$PRGNAM-$VERSION}
cd $TMP; tar xvf $CWD/$PRGNAM-$VERSION.tar.?z*
cd $PRGNAM-$VERSION
chown -R root:root .
chmod -R u+w,go+r-w,a+X-s .

CFLAGS="$SLKCFLAGS" \
./configure \
  --prefix=/usr \
  --sysconfdir=/etc \
  --localstatedir=/var/lib \
  --mandir=/usr/man \
  --disable-static
make; make install-strip DESTDIR=$PKG

cp -a AUTHORS COPYING ChangeLog INSTALL NEWS README TODO $PKG/usr/doc/$PRGNAM-$VERSION

echo "       |-----handy-ruler------------------------------------------------------|
fuseiso: fuseiso (FUSE module to mount ISO filesystem images)
fuseiso:
fuseiso: FuseISO is a FUSE module to mount ISO filesystem images (.iso, .nrg,
fuseiso: .bin, .mdf and .img files). It currently support plain ISO9660 Level
fuseiso: 1 and 2, Rock Ridge, Joliet, and zisofs.
fuseiso:
fuseiso:
fuseiso:
fuseiso:
fuseiso: https://sourceforge.net/projects/fuseiso/
fuseiso:" > $PKG/install/slack-desc

cd $PKG; /sbin/makepkg -l y -c n $CWD/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.txz
chown -R 1000:users $CWD/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.txz
rm -rf $PKG $TMP/$PRGNAM-$VERSION

cd $CWD
[ ${TIME:-0} != 0 ] && TIME="-t $TIME" || TIME=
if [ "${INST:-no}" = "yes" ]; then
	OPTION=y
else
	[ -e "../Exclamation.mp3" ] && pkexec env DISPLAY=$DISPLAY XAUTHORITY=$XAUTHORITY mplayer $WD/Exclamation.mp3 > /dev/null
	read $TIME -p "O pacote já pode ser instalado? (y/n) (default=n)" OPTION
fi
case "$OPTION" in
	y|Y) /sbin/upgradepkg --install-new --reinstall $PRGNAM-$VERSION-$ARCH-$BUILD$TAG.txz ;;
esac; exit 0
