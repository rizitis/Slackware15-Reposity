#!/bin/sh

# Slackware build script for yad

# Copyright 2021 Mauricio Ferrari <m10ferrari1200@gmail.com>
# All rights reserved.

[ $UID != 0 ] && { echo -e "\nExecute como Root !\n"; exit 1; }

PRGNAM=yad
VERSION=10.1
BUILD=${BUILD:-1}
TAG=${TAG:-_mxnt}

case "$( uname -m )" in
	i?86) ARCH=i586 ;;
	arm*) ARCH=arm ;;
	   *) ARCH=$( uname -m ) ;;
esac

WD=$PWD; mkdir -p $PRGNAM; chown -R 1000:users $PRGNAM; cd $PRGNAM 

CWD=$PWD
TMP=/tmp
PKG=$TMP/package-$PRGNAM
LINK=https://github.com/v1cont/yad/releases/download/v10.1/yad-10.1.tar.xz

if [ "$ARCH" = "i586" ]; then
	SLKCFLAGS="-O2 -march=i586 -mtune=i686"
	LIBDIRSUFFIX=""
elif [ "$ARCH" = "i686" ]; then
	SLKCFLAGS="-O2 -march=i686 -mtune=i686"
	LIBDIRSUFFIX=""
elif [ "$ARCH" = "aarch64" ]; then
	SLKCFLAGS="-O2"
	LIBDIRSUFFIX="64"
elif [ "$ARCH" = "x86_64" ]; then
	SLKCFLAGS="-O2 -fPIC"
	LIBDIRSUFFIX="64"
else
	SLKCFLAGS="-O2"
	LIBDIRSUFFIX=""
fi

set -e
wget -c $LINK
rm -rf $PKG $TMP/$PRGNAM-$VERSION
mkdir -p $PKG/{install,usr/doc/$PRGNAM-$VERSION}
cd $TMP; tar xvf $CWD/$PRGNAM-$VERSION.tar.?z*
cd $PRGNAM-$VERSION
chown -R root:root .
chmod -R u+w,go+r-w,a+X-s .

CFLAGS="$SLKCFLAGS" \
CXXFLAGS="$SLKCFLAGS" \
./configure \
  --prefix=/usr \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --mandir=/usr/man \
  --docdir=/usr/doc/$PRGNAM-$VERSION \
  --with-rgb=/usr/share/X11/rgb.txt \
  --enable-icon-browser \
  --enable-html \
  --build=$ARCH-slackware-linux
make; make install-strip DESTDIR=$PKG

find $PKG/usr/man -type f -exec gzip -9 {} \;
for i in $( find $PKG/usr/man -type l ) ; do ln -s $( readlink $i ).gz $i.gz ; rm $i ; done
cp -a AUTHORS NEWS README* $PKG/usr/doc/$PRGNAM-$VERSION

echo "   |-----handy-ruler------------------------------------------------------|
yad: yad (displays graphical dialogs from shell scripts or command line)
yad:
yad: Yad (yet another dialog) is a fork of Zenity with many improvements,
yad: including custom buttons, additional dialogs and a pop-up menu in
yad: the notification area.
yad:
yad: There were two main reasons for this fork.  The first was to remove
yad: dependencies on deprecated libraries like libglade and gnome-canvas.
yad: The second was the slow pace of Zenity development including many
yad: unimplemented enhancement suggestions in the GNOME Bugzilla.
yad:" > $PKG/install/slack-desc

echo "if [ -x /usr/bin/update-desktop-database ]; then
	/usr/bin/update-desktop-database -q usr/share/applications >/dev/null 2>&1
fi

if [ -x /usr/bin/update-mime-database ]; then
	/usr/bin/update-mime-database usr/share/mime >/dev/null 2>&1
fi

if [ -e usr/share/icons/hicolor/icon-theme.cache ]; then
	if [ -x /usr/bin/gtk-update-icon-cache ]; then
		/usr/bin/gtk-update-icon-cache usr/share/icons/hicolor >/dev/null 2>&1
	fi
fi

if [ -e usr/share/glib-2.0/schemas ]; then
	if [ -x /usr/bin/glib-compile-schemas ]; then
		/usr/bin/glib-compile-schemas usr/share/glib-2.0/schemas >/dev/null 2>&1
	fi
fi" > $PKG/install/doinst.sh

cd $PKG; /sbin/makepkg -l y -c n $CWD/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.txz
chown -R 1000:users $CWD/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.txz
rm -rf $PKG $TMP/$PRGNAM-$VERSION

cd $CWD
[ ${TIME:-0} != 0 ] && TIME="-t $TIME" || TIME=
if [ "${INST:-no}" = "yes" ]; then
	OPTION=y
else
	[ -e "../Exclamation.mp3" ] && pkexec env DISPLAY=$DISPLAY XAUTHORITY=$XAUTHORITY mplayer $WD/Exclamation.mp3 > /dev/null
	read $TIME -p "O pacote já pode ser instalado? (y/n) (default=n)" OPTION
fi
case "$OPTION" in
	y|Y) /sbin/upgradepkg --install-new --reinstall $PRGNAM-$VERSION-$ARCH-$BUILD$TAG.txz ;;
esac; exit 0
