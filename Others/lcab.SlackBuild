#!/bin/sh

# Slackware build script for lcab

# Copyright 2021 Mauricio Ferrari <m10ferrari1200@gmail.com>
# All rights reserved.

[ $UID != 0 ] && { echo -e "\nExecute como Root !\n"; exit 1; }

PRGNAM=lcab
VERSION=1.0b12
BUILD=${BUILD:-1}
TAG=${TAG:-_mxnt}

case "$( uname -m )" in
	i?86) ARCH=i586 ;;
	arm*) ARCH=arm ;;
	   *) ARCH=$( uname -m ) ;;
esac

WD=$PWD; mkdir -p $PRGNAM; chown -R 1000:users $PRGNAM; cd $PRGNAM

CWD=$PWD
TMP=/tmp
PKG=$TMP/package-$PRGNAM
LINK=http://mirrors.gigenet.com/archlinux/other/lcab/lcab-1.0b12.tar.gz

set -e
wget -c $LINK
rm -rf $PKG $TMP/$PRGNAM-$VERSION
mkdir -p $PKG/{install,usr/{bin,man/man1,doc/$PRGNAM-$VERSION}}
cd $TMP; tar xvf $CWD/$PRGNAM-$VERSION.tar.?z*
cd $PRGNAM-$VERSION
chown -R root:root .
chmod -R u+w,go+r-w,a+X-s .

make
install -Dpm755 'lcab' "$PKG/usr/bin/lcab"
cp -a COPYING README $PKG/usr/doc/$PRGNAM-$VERSION
cp -a lcab.1 $PKG/usr/man/man1
find $PKG/usr/man -type f -exec gzip -9 {} \;
for i in $( find $PKG/usr/man -type l ) ; do ln -s $( readlink $i ).gz $i.gz ; rm $i ; done

echo "    |-----handy-ruler------------------------------------------------------|
lcab: lcab (A program to make Microsoft cabinet files)
lcab:
lcab: Cabinet Files specs from Microsoft.
lcab:
lcab:
lcab:
lcab:
lcab:
lcab:
lcab:
lcab:" > $PKG/install/slack-desc

cd $PKG; /sbin/makepkg -l y -c n $CWD/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.txz
chown -R 1000:users $CWD/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.txz
rm -rf $PKG $TMP/$PRGNAM-$VERSION

cd $CWD
[ ${TIME:-0} != 0 ] && TIME="-t $TIME" || TIME=
if [ "${INST:-no}" = "yes" ]; then
	OPTION=y
else
	[ -e "../Exclamation.mp3" ] && pkexec env DISPLAY=$DISPLAY XAUTHORITY=$XAUTHORITY mplayer $WD/Exclamation.mp3 > /dev/null
	read $TIME -p "O pacote já pode ser instalado? (y/n) (default=n)" OPTION
fi
case "$OPTION" in
	y|Y) /sbin/upgradepkg --install-new --reinstall $PRGNAM-$VERSION-$ARCH-$BUILD$TAG.txz ;;
esac; exit 0
