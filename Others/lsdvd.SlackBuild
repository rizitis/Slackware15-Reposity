#!/bin/sh

# Slackware build script for lsdvd

# Copyright 2021 Mauricio Ferrari <m10ferrari1200@gmail.com>
# All rights reserved.

[ $UID != 0 ] && { echo -e "\nExecute como Root !\n"; exit 1; }

PRGNAM=lsdvd
VERSION=0.17
BUILD=${BUILD:-1}
TAG=${TAG:-_mxnt}

case "$( uname -m )" in
	i?86) export ARCH=i586 ;;
	arm*) export ARCH=arm ;;
	   *) export ARCH=$( uname -m ) ;;
esac

WD=$PWD; mkdir -p $PRGNAM; chown -R 1000:users $PRGNAM; cd $PRGNAM 

CWD=$PWD
TMP=/tmp
PKG=$TMP/package-$PRGNAM
LINK=https://ufpr.dl.sourceforge.net/project/lsdvd/lsdvd/lsdvd-0.17.tar.gz

if [ "$ARCH" = "i586" ]; then
	SLKCFLAGS="-O2 -march=i586 -mtune=i686"
	LIBDIRSUFFIX=""
elif [ "$ARCH" = "i686" ]; then
	SLKCFLAGS="-O2 -march=i686 -mtune=i686"
	LIBDIRSUFFIX=""
elif [ "$ARCH" = "aarch64" ]; then
	SLKCFLAGS="-O2"
	LIBDIRSUFFIX="64"
elif [ "$ARCH" = "x86_64" ]; then
	SLKCFLAGS="-O2 -fPIC"
	LIBDIRSUFFIX="64"
else
	SLKCFLAGS="-O2"
	LIBDIRSUFFIX=""
fi

set -e
wget -c $LINK
rm -rf $PKG $TMP/$PRGNAM-$VERSION
mkdir -p $PKG/{install,usr/doc/$PRGNAM-$VERSION}
cd $TMP; tar xvf $CWD/$PRGNAM-$VERSION.tar.gz
cd $PRGNAM-$VERSION
chown -R root:root .
chmod -R u+w,go+r-w,a+X-s .

CFLAGS="$SLKCFLAGS" \
CXXFLAGS="$SLKCFLAGS" \
./configure \
  --prefix=/usr \
  --libdir=/usr/lib$LIBDIRSUFFIX \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --mandir=/usr/man
make; make install-strip DESTDIR=$PKG

gzip -9 $PKG/usr/man/man1/$PRGNAM.1
cp ChangeLog AUTHORS COPYING INSTALL README $PKG/usr/doc/$PRGNAM-$VERSION

echo "     |-----handy-ruler------------------------------------------------------|
lsdvd: lsdvd (list the contents of a DVD)
lsdvd:
lsdvd: lsdvd is a console application that displays the content of a dvd. It
lsdvd: provides output about the types of video and audio tracks, subtitles
lsdvd: etc... output is available in a number of formats including human
lsdvd: readable, perl, ruby or xml.
lsdvd:
lsdvd:
lsdvd:
lsdvd:
lsdvd:" > $PKG/install/slack-desc

cd $PKG; /sbin/makepkg -l y -c n $CWD/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.txz
chown -R 1000:users $CWD/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.txz
rm -rf $PKG $TMP/$PRGNAM-$VERSION

cd $CWD
[ ${TIME:-0} != 0 ] && TIME="-t $TIME" || TIME=
if [ "${INST:-no}" = "yes" ]; then
	OPTION=y
else
	[ -e "../Exclamation.mp3" ] && pkexec env DISPLAY=$DISPLAY XAUTHORITY=$XAUTHORITY mplayer $WD/Exclamation.mp3 > /dev/null
	read $TIME -p "O pacote já pode ser instalado? (y/n) (default=n)" OPTION
fi
case "$OPTION" in
	y|Y) /sbin/upgradepkg --install-new --reinstall $PRGNAM-$VERSION-$ARCH-$BUILD$TAG.txz ;;
esac; exit 0
