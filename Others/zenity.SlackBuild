#!/bin/sh

# Slackware build script for zenity

# Copyright 2021 Mauricio Ferrari <m10ferrari1200@gmail.com>
# All rights reserved.

[ $UID != 0 ] && { echo -e "\nExecute como Root !\n"; exit 1; }

PRGNAM=zenity
VERSION=3.32.0
BUILD=${BUILD:-1}
TAG=${TAG:-_mxnt}

case "$( uname -m )" in
	i?86) export ARCH=i586 ;;
	arm*) export ARCH=arm ;;
	   *) export ARCH=$( uname -m ) ;;
esac

WD=$PWD; mkdir -p $PRGNAM; chown -R 1000:users $PRGNAM; cd $PRGNAM 

CWD=$PWD
TMP=/tmp
PKG=$TMP/package-$PRGNAM
LINK=https://download.gnome.org/sources/zenity/3.32/zenity-3.32.0.tar.xz

if [ "$ARCH" = "i586" ]; then
	SLKCFLAGS="-O2 -march=i586 -mtune=i686"
	LIBDIRSUFFIX=""
elif [ "$ARCH" = "i686" ]; then
	SLKCFLAGS="-O2 -march=i686 -mtune=i686"
	LIBDIRSUFFIX=""
elif [ "$ARCH" = "aarch64" ]; then
	SLKCFLAGS="-O2"
	LIBDIRSUFFIX="64"
elif [ "$ARCH" = "x86_64" ]; then
	SLKCFLAGS="-O2 -fPIC -pipe -fomit-frame-pointer"
	LIBDIRSUFFIX="64"
else
	SLKCFLAGS="-O2"
	LIBDIRSUFFIX=""
fi

set -e
wget -c $LINK
rm -rf $PKG $TMP/$PRGNAM-$VERSION
mkdir -p $PKG/{install,usr/doc/$PRGNAM-$VERSION}
cd $TMP; tar xvf $CWD/$PRGNAM-$VERSION.tar.xz
cd $PRGNAM-$VERSION
chown -R root:root .
chmod -R u+w,go+r-w,a+X-s .

LDFLAGS="-L/usr/lib${LIBDIRSUFFIX}" \
CFLAGS="$SLKCFLAGS" \
CXXFLAGS="$SLKCFLAGS" \
./configure \
  --prefix=/usr \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --mandir=/usr/man \
  --infodir=/usr/info \
  --docdir=/usr/doc/$PRGNAM-$VERSION \
  --enable-debug=no \
  --enable-libnotify \
  --host=$ARCH-slackware-linux \
  --build=$ARCH-slackware-linux
make; make install-strip DESTDIR=$PKG

rm -f $PKG/{,usr/}lib${LIBDIRSUFFIX}/*.la
find $PKG/usr/man -type f -exec chmod 644 {} \; -exec gzip -9 {} \;
for i in $( find $PKG/usr/man -type l ) ; do ln -s $( readlink $i ).gz $i.gz ; rm $i ; done
cp -a AUTHORS ChangeLog COPYING HACKING NEWS README THANKS TODO $PKG/usr/doc/$PRGNAM-$VERSION

echo '      |-----handy-ruler------------------------------------------------------|
zenity: zenity (display gtk dialog boxes from cli)
zenity:
zenity: Zenity is a tool that allows you to display Gtk+ dialog boxes from
zenity: the command line and through shell scripts. It is similar to
zenity: gdialog, but is intended to be saner.
zenity:
zenity:
zenity:
zenity:
zenity:
zenity:' > $PKG/install/slack-desc

cd $PKG; /sbin/makepkg -l y -c n $CWD/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.txz
chown -R 1000:users $CWD/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.txz
rm -rf $PKG $TMP/$PRGNAM-$VERSION

cd $CWD
[ ${TIME:-0} != 0 ] && TIME="-t $TIME" || TIME=
if [ "${INST:-no}" = "yes" ]; then
	OPTION=y
else
	[ -e "../Exclamation.mp3" ] && pkexec env DISPLAY=$DISPLAY XAUTHORITY=$XAUTHORITY mplayer $WD/Exclamation.mp3 > /dev/null
	read $TIME -p "O pacote já pode ser instalado? (y/n) (default=n)" OPTION
fi
case "$OPTION" in
	y|Y) /sbin/upgradepkg --install-new --reinstall $PRGNAM-$VERSION-$ARCH-$BUILD$TAG.txz ;;
esac; exit 0
