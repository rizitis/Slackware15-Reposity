#!/bin/sh
# Software

# Slackware build script for protonvpn-cli

# Copyright 2021 Mauricio Ferrari <m10ferrari1200@gmail.com>
# All rights reserved.

[ $UID != 0 ] && { echo -e "\nExecute como Root !\n"; exit 1; }
[ ! "$(ls /var/log/packages/python3-docopt*       2>&-)" ] && { echo -e "\nDependência Faltante: python3-docopt !      \n"; ERRO=1; }
[ ! "$(ls /var/log/packages/python3-Jinja2*       2>&-)" ] && { echo -e "\nDependência Faltante: python3-Jinja2 !      \n"; ERRO=1; }
[ ! "$(ls /var/log/packages/python3-pythondialog* 2>&-)" ] && { echo -e "\nDependência Faltante: python3-pythondialog !\n"; ERRO=1; }
[ "$ERRO" = 1 ] && exit 1

PRGNAM=protonvpn-cli
SRCNAM=linux-cli
VERSION=2.2.11
BUILD=${BUILD:-1}
TAG=${TAG:-_mxnt}

case "$( uname -m )" in
	i?86) ARCH=i586 ;;
	arm*) ARCH=arm ;;
	   *) ARCH=$( uname -m ) ;;
esac

WD=$PWD; mkdir -p $PRGNAM; chown -R 1000:users $PRGNAM; cd $PRGNAM 

CWD=$PWD
TMP=/tmp
PKG=$TMP/package-$PRGNAM
LINK=https://github.com/ProtonVPN/linux-cli-community/archive/refs/tags/v2.2.11.tar.gz

set -e
wget -c -O $SRCNAM-$VERSION.tar.gz $LINK
rm -rf $PKG $TMP/$SRCNAM-community-$VERSION
mkdir -p $PKG/{install,usr/doc/$PRGNAM-$VERSION}
cd $TMP; tar xvf $CWD/$SRCNAM-$VERSION.tar.?z*
cd $SRCNAM-community-$VERSION
chown -R root:root .
chmod -R u+w,go+r-w,a+X-s .

python3 setup.py install --root=$PKG

cp -a AUTHORS CHANGELOG.md LICENSE README.md USAGE.md $PKG/usr/doc/$PRGNAM-$VERSION

echo "             |-----handy-ruler------------------------------------------------------|
protonvpn-cli: protonvpn-cli (ProtonVPN's Linux Command-Line Interface)
protonvpn-cli:
protonvpn-cli: Official Linux command line tool for connection to the ProtonVPN. It
protonvpn-cli: is a full rewrite of the original bash protonvpn-cli in Python, which
protonvpn-cli: adds more features and functionality with the purpose of improving
protonvpn-cli: readability, speed, and reliability.
protonvpn-cli:
protonvpn-cli: Homepage: https://protonvpn.com/support/linux-vpn-tool/
protonvpn-cli:
protonvpn-cli:
protonvpn-cli:" > $PKG/install/slack-desc

cd $PKG; /sbin/makepkg -l y -c n $CWD/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.txz
chown -R 1000:users $CWD/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.txz
rm -rf $PKG $TMP/$SRCNAM-community-$VERSION

cd $CWD
[ ${TIME:-0} != 0 ] && TIME="-t $TIME" || TIME=
if [ "${INST:-no}" = "yes" ]; then
	OPTION=y
else
	[ -e "../Exclamation.mp3" ] && pkexec env DISPLAY=$DISPLAY XAUTHORITY=$XAUTHORITY mplayer $WD/Exclamation.mp3 > /dev/null
	read $TIME -p "O pacote já pode ser instalado? (y/n) (default=n)" OPTION
fi
case "$OPTION" in
	y|Y) /sbin/upgradepkg --install-new --reinstall $PRGNAM-$VERSION-$ARCH-$BUILD$TAG.txz ;;
esac; exit 0
