#!/bin/sh
# Dependencies

# Slackware build script for python3-xlib

# Copyright 2021 Mauricio Ferrari <m10ferrari1200@gmail.com>
# All rights reserved.

[ $UID != 0 ] && { echo -e "\nExecute como Root !\n"; exit 1; }

PRGNAM=python3-xlib
SRCNAM=python-xlib
VERSION=0.31
BUILD=${BUILD:-1}
TAG=${TAG:-_mxnt}

case "$( uname -m )" in
	i?86) ARCH=i586 ;;
	arm*) ARCH=arm ;;
	   *) ARCH=$( uname -m ) ;;
esac

WD=$PWD; mkdir -p $PRGNAM; chown -R 1000:users $PRGNAM; cd $PRGNAM

CWD=$PWD
TMP=/tmp
PKG=$TMP/package-$PRGNAM
LINK1=https://github.com/python-xlib/python-xlib/releases/download/0.31/python-xlib-0.31.tar.bz2

set -e
wget -c $LINK1
rm -rf $PKG $TMP/$SRCNAM-$VERSION
mkdir -p $PKG/{install,usr/doc/$PRGNAM-$VERSION}
cd $TMP; tar xvf $CWD/$SRCNAM-$VERSION.tar.?z*
cd $SRCNAM-$VERSION
chown -R root:root .
chmod -R u+w,go+r-w,a+X-s .

python3 setup.py install --root=$PKG
cp -a CHANGELOG.md LICENSE PKG-INFO README.rst TODO $PKG/usr/doc/$PRGNAM-$VERSION

echo '            |-----handy-ruler------------------------------------------------------|
python3-xlib: python3-xlib (X Library module for Python)
python3-xlib:
python3-xlib: The Python X Library is intended to be a fully functional X client
python3-xlib: library for Python programs. It is written entirely in Python, in
python3-xlib: contrast to earlier X libraries for Python which were interfaces to
python3-xlib: the C Xlib. The communication takes place over TCP/IP, Unix sockets,
python3-xlib: DECnet or any other streaming network protocol. The C Xlib is merely
python3-xlib: an interface to this protocol, providing functions suitable for a C
python3-xlib: environment.
python3-xlib:
python3-xlib: Homepage: https://github.com/python-xlib/python-xlib' > $PKG/install/slack-desc

cd $PKG; /sbin/makepkg -l y -c n $CWD/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.txz
chown -R 1000:users $CWD/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.txz
rm -rf $PKG $TMP/$SRCNAM-$VERSION

cd $CWD
[ ${TIME:-0} != 0 ] && TIME="-t $TIME" || TIME=
if [ "${INST:-no}" = "yes" ]; then
	OPTION=y
else
	[ -e "../Exclamation.mp3" ] && pkexec env DISPLAY=$DISPLAY XAUTHORITY=$XAUTHORITY mplayer $WD/Exclamation.mp3 > /dev/null
	read $TIME -p "O pacote já pode ser instalado? (y/n) (default=n)" OPTION
fi
case "$OPTION" in
	y|Y) /sbin/upgradepkg --install-new --reinstall $PRGNAM-$VERSION-$ARCH-$BUILD$TAG.txz ;;
esac; exit 0
