# Slackware15-Reposity
*Reposity for Slackware 15.0 Stable.*
<br><br>

Welcome to the package repository for Slackware 15.0 Stable.<br>
Feel at ease to install the packages you need.
<br><br>

## Softwares Available in this Repository:
<br>

| `Softwares Packages`       | `Version`        | `Architecture`          | `Required By`           |
|----------------------------|------------------|-------------------------|-------------------------|
| `acetoneiso`               | `2.4`            | `i586` `x86_64` `arm`   |                         |
| `codeblocks`               | `20.03`          | `i586` `x86_64` `arm`   |                         |
| `conky-manager`            | `2.7`            | `i586` `x86_64` `arm`   |                         |
| `CPU-X`                    | `4.2.0`          | `i586` `x86_64` `arm`   |                         |
| `flareget`                 | `5.0.0`          | `i586` `x86_64`         |                         |
| `freeoffice2021`           | `1036`           | `x86_64`                |                         |
| `google-chrome`            | `97.0.4692.71`   | `x86_64`                |                         |
| `google-earth`             | `7.3.3.7786`     | `x86_64`                |                         |
| `mpv`                      | `0.34.0`         | `i586` `x86_64` `arm`   | `hypnotix`              |
| `mx-conky`                 | `21.7`           | `i586` `x86_64` `arm`   |                         |
| `nulloy`                   | `0.8.2_112`      | `i586` `x86_64` `arm`   |                         |
| `ocenaudio`                | `3.11.2`         | `i586` `x86_64`         |                         |
| `p7zip`                    | `17.04`          | `i586` `x86_64` `arm`   | `acetoneiso`            |
| `photoqt`                  | `2.4`            | `i586` `x86_64` `arm`   |                         |
| `qbittorrent`              | `4.3.7`          | `i586` `x86_64` `arm`   |                         |
| `QMPlay2`                  | `21.06.07`       | `i586` `x86_64` `arm`   |                         |
| `spotify`                  | `1.1.72.439`     | `x86_64`                |                         |
| `telegram`                 | `3.4.3`          | `x86_64`                |                         |
| `uget`                     | `2.2.3`          | `i586` `x86_64` `arm`   |                         |
| `umplayer`                 | `0.98.2`         | `x86_64`                |                         |
| `........................` | `..............` | `.....................` | `.....................` |

| `Dependencies Packages`    | `Version`        | `Architecture`          | `Required By`           |
|----------------------------|------------------|-------------------------|-------------------------|
| `aria2`                    | `1.36.0`         | `i586` `x86_64` `arm`   | `uget`                  |
| `cdrkit`                   | `1.1.11`         | `i586` `x86_64` `arm`   | `acetoneiso`            |
| `conky`                    | `1.10.8`         | `i586` `x86_64` `arm`   | `conky-manager`         |
| `fuseiso`                  | `20070708`       | `i586` `x86_64` `arm`   | `acetoneiso`            |
| `graphicsmagick`           | `1.3.37`         | `i586` `x86_64` `arm`   | `photoqt`               |
| `imlib2`                   | `1.7.1`          | `i586` `x86_64` `arm`   | `conky`                 |
| `jack`                     | `1.9.19`         | `i586` `x86_64` `arm`   | `ocenaudio` `portaudio` |
| `libass`                   | `0.15.2`         | `i586` `x86_64` `arm`   | `mpv` `QMPlay2`         |
| `libcpuid`                 | `0.5.1`          | `i586` `x86_64` `arm`   | `CPU-X`                 |
| `libgme`                   | `0.6.3`          | `i586` `x86_64` `arm`   | `QMPlay2`               |
| `libplacebo`               | `3.120.3`        | `i586` `x86_64` `arm`   | `mpv`                   |
| `libqpsd`                  | `2.2.1`          | `i586` `x86_64` `arm`   | `photoqt`               |
| `libsidplayfp`             | `2.3.1`          | `i586` `x86_64` `arm`   | `QMPlay2`               |
| `libtorrent-rasterbar`     | `2.0.4`          | `i586` `x86_64` `arm`   | `qbittorrent`           |
| `lua`                      | `5.1.5`          | `i586` `x86_64` `arm`   | `mpv` `tolua++`         |
| `mujs`                     | `1.1.3`          | `i586` `x86_64` `arm`   | `mpv`                   |
| `portaudio`                | `19.7.0`         | `i586` `x86_64` `arm`   | `QMPlay2` `espeak`      |
| `pugixml`                  | `1.11.4`         | `i586` `x86_64` `arm`   | `photoqt`               |
| `rar`                      | `6.0.2`          | `x86_64`                | `acetoneiso`            |
| `tolua++`                  | `1.0.93`         | `i586` `x86_64` `arm`   | `conky`                 |
| `wxGTK3`                   | `3.0.5.1`        | `i586` `x86_64` `arm`   | `codeblocks` `p7zip`    |
| `xa`                       | `2.3.12`         | `i586` `x86_64` `arm`   | `libsidplayfp`          |
| `........................` | `..............` | `.....................` | `.....................` |

<br>

| `Electron Packages`        | `Version`        | `Architecture`          | `Required By`           |
|----------------------------|------------------|-------------------------|-------------------------|
| `discord`                  | `0.0.16`         | `x86_64`                |                         |
| `iptvnator`                | `0.9.0`          | `x86_64`                |                         |
| `notable`                  | `1.8.4`          | `x86_64`                |                         |
| `vscodium`                 | `1.63.2`         | `x86_64`                |                         |
| `........................` | `..............` | `.....................` | `.....................` |

<br>

| `Games Packages`           | `Version`        | `Architecture`          | `Required By`           |
|----------------------------|------------------|-------------------------|-------------------------|
| `flare-game`               | `1.13`           | `i586` `x86_64` `arm`   |                         |
| `opensonic`                | `0.1.4`          | `i586` `x86_64` `arm`   |                         |
| `quake2`                   | `8.00`           | `i586` `x86_64` `arm`   |                         |
| `smc`                      | `20140328`       | `i586` `x86_64` `arm`   |                         |
| `........................` | `..............` | `.....................` | `.....................` |

| `Dependencies Packages`    | `Version`        | `Architecture`          | `Required By`           |
|----------------------------|------------------|-------------------------|-------------------------|
| `allegro4`                 | `4.4.2`          | `i586` `x86_64` `arm`   | `opensonic`             |
| `CEGUI07`                  | `0.7.9`          | `i586` `x86_64` `arm`   | `smc`                   |
| `FreeImage`                | `3.18.0`         | `i586` `x86_64` `arm`   | `CEGUI07`               |
| `........................` | `..............` | `.....................` | `.....................` |

<br>

| `Java Packages`            | `Version`        | `Architecture`          | `Required By`           |
|----------------------------|------------------|-------------------------|-------------------------|
| `clion`                    | `2021.3`         | `noarch`                |                         |
| `frostwire`                | `6.9.6`          | `x86_64`                |                         |
| `jdk17`                    | `17.0.1`         | `x86_64`                |                         |
| `JDownloader2`             | `2.0`            | `noarch`                |                         |
| `pycharm`                  | `2021.3`         | `noarch`                |                         |
| `........................` | `..............` | `.....................` | `.....................` |

<br>

| `Python Packages`          | `Version`        | `Architecture`          | `Required By`           |
|----------------------------|------------------|-------------------------|-------------------------|
| `face`                     | `2.2`            | `noarch`                |                         |
| `hypnotix`                 | `2.6`            | `noarch`                |                         |
| `kazam`                    | `1.5.4`          | `noarch`                |                         |
| `protonvpn-cli`            | `2.2.11`         | `i586` `x86_64` `arm`   | `protonvpn-gui`         |
| `protonvpn-gui`            | `2.1.1`          | `i586` `x86_64` `arm`   |                         |
| `swapfile`                 | `1.1`            | `noarch`                |                         |
| `transmageddon`            | `1.5`            | `noarch`                |                         |
| `whats`                    | `2.2`            | `noarch`                |                         |
| `youtube-dl`               | `2021.12.17`     | `i586` `x86_64` `arm`   | `hypnotix`              |
| `........................` | `..............` | `.....................` | `.....................` |

| `Dependencies Packages`    | `Version`        | `Architecture`          | `Required By`           |
|----------------------------|------------------|-------------------------|-------------------------|
| `BeautifulSoup4`           | `4.10.0`         | `i586` `x86_64` `arm`   | `whats`                 |
| `python3-ConfigParser`     | `5.2.0`          | `i586` `x86_64` `arm`   | `protonvpn-gui`         |
| `python3-distutils-extra`  | `2.39`           | `i586` `x86_64` `arm`   | `kazam`                 |
| `python3-docopt`           | `0.6.2`          | `i586` `x86_64` `arm`   | `protonvpn-cli`         |
| `python3-imdbpy`           | `2021.4.18`      | `i586` `x86_64` `arm`   | `hypnotix`              |
| `python3-Jinja2`           | `3.0.3`          | `i586` `x86_64` `arm`   | `protonvpn-cli`         |
| `python3-lxml`             | `4.7.1`          | `i586` `x86_64` `arm`   | `face`                  |
| `python3-MarkupSafe`       | `2.0.1`          | `i586` `x86_64` `arm`   | `python3-Jinja2`        |
| `python3-pythondialog`     | `3.5.3`          | `i586` `x86_64` `arm`   | `protonvpn-cli`         |
| `python3-setproctitle`     | `1.2.2`          | `i586` `x86_64` `arm`   | `hypnotix`              |
| `python3-soupsieve`        | `2.3.1`          | `i586` `x86_64` `arm`   | `BeautifulSoup4` `face` `whats` |
| `python3-Unidecode`        | `1.3.2`          | `i586` `x86_64` `arm`   | `hypnotix`              |
| `python3-xlib`             | `0.31`           | `i586` `x86_64` `arm`   | `kazam`                 |
| `........................` | `..............` | `.....................` | `.....................` |

<br>

| `Others Packages`          | `Version`        | `Architecture`          | `Required By`           |
|----------------------------|------------------|-------------------------|-------------------------|
| `asciiquarium`             | `1.1`            | `noarch`                |                         |
| `cabextract`               | `1.9.1`          | `i586` `x86_64` `arm`   |                         |
| `ccat`                     | `1.1.0`          | `i586` `x86_64` `arm`   |                         |
| `cmatrix`                  | `2.0`            | `i586` `x86_64` `arm`   |                         |
| `cowsay`                   | `3.04`           | `noarch`                |                         |
| `dpkg`                     | `1.21.1`         | `i586` `x86_64` `arm`   |                         |
| `espeak`                   | `1.48.04`        | `i586` `x86_64` `arm`   |                         |
| `grc`                      | `1.13`           | `i586` `x86_64` `arm`   |                         |
| `gst-plugins-bad`          | `1.18.4`         | `i586` `x86_64` `arm`   | `nulloy` `transmageddon`  |
| `gst-plugins-ugly`         | `1.18.4`         | `i586` `x86_64` `arm`   | `nulloy` `transmageddon`  |
| `gtkdialog`                | `0.8.4`          | `i586` `x86_64` `arm`   |                         |
| `lcab`                     | `1.0b12`         | `i586` `x86_64` `arm`   |                         |
| `libgnomekbd`              | `3.26.1`         | `i586` `x86_64` `arm`   | `xapp`                  |
| `lsdvd`                    | `0.17`           | `i586` `x86_64` `arm`   | `transmageddon`         |
| `luna-wallpapers`          | `0.2`            | `noarch`                |                         |
| `mandriva-cursor`          | `2.0`            | `noarch`                |                         |
| `molotov`                  | `1.2`            | `noarch`                |                         |
| `nosync-browser`           | `1.0`            | `noarch`                |                         |
| `perl-Curses`              | `1.38`           | `i586` `x86_64` `arm`   | `perl-Term-Animation`   |
| `perl-Term-Animation`      | `2.6`            | `i586` `x86_64` `arm`   | `asciiquarium`          |
| `refine`                   | `3.1`            | `i586` `x86_64` `arm`   |                         |
| `sl`                       | `5.02`           | `i586` `x86_64` `arm`   |                         |
| `w3m`                      | `0.5.3_37`       | `i586` `x86_64` `arm`   |                         |
| `xapp`                     | `2.2.1`          | `i586` `x86_64` `arm`   | `hypnotix`              |
| `yad`                      | `10.1`           | `i586` `x86_64` `arm`   |                         |
| `zenity`                   | `3.32.0`         | `i586` `x86_64` `arm`   |                         |
| `........................` | `..............` | `.....................` | `.....................` |

<br>

| `Specific Packages`        | `Version`        | `Architecture`          | `Required By`           |
|----------------------------|------------------|-------------------------|-------------------------|
| `fortune-mod-br`           | `20160820`       | `noarch`                |                         |
| `fortune-mod`              | `2.12.0`         | `i586` `x86_64` `arm`   | `fortune-mod-br`        |
| `recode`                   | `3.7.9`          | `i586` `x86_64` `arm`   | `fortune-mod`           |
| `vim`                      | `8.2.3444`       | `i586` `x86_64` `arm`   |                         |
| `........................` | `..............` | `.....................` | `.....................` |

### GNU General Public License

This repository has scripts that were created to be free software.<br/>
Therefore, they can be distributed and / or modified within the terms of the *GNU General Public License*.

>[General Public License](https://pt.wikipedia.org/wiki/GNU_General_Public_License)
>
>Free Software Foundation (FSF) Inc. 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

### Comments

In case of bugs, execution problems or packages construction, constructive criticism, among others, please submit a message to one of the contacts below.
<br/>

### Contact

Autor: Mauricio Ferrari

E-Mail: *m10ferrari1200@gmail.com*

Telegram: *@maurixnovatrento*

